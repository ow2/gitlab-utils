##Howto

- Retrieve the ID of the project in GForge
- On a web browser, connect to a PhpPgAdmin console, e.g. https://myserver/phpPgAdmin/sql.php
- Select the GForge database, then click on the "SQL" tab.
- For each request in the file export-queries.sql:
  - Run the request in PhpPgAdmin
  - Export the results in XML format.
	- Rename the file like %GFORGE_PROJECT%\_[authors, trackers, comments or attachments].xml, in the same directory as the Python script GforgeIssuesToGitLabSqlGenerator.py
- In GforgeIssuesToGitLabSqlGenerator.py: change GFORGE_PROJECT and GITLAB_PROJECT. Set GITLAB_PROJECT to None if the GitLab project isn't created, the script will create it.
- GITLAB_URL contains the URL of the GitLab instance to modify.
- GITLAB_TOKEN is the 'private token' for the administrator from Gitlab instance.
- GITLAB_USER_NAMES may have to be changed.
- The created users have the following credentials: their GForge username as username, username+'p4ssWd' as password.
- Run GforgeIssuesToGitLabSqlGenerator.py
- Run the following command: ```scp %GFORGE_PROJECT%.sql %username%@%gitlabServer%:/%destination%```
- On the GitLab server run the following commands:
```
cd /opt/gitlab
sudo -u gitlab-psql ./embedded/bin/psql -h /var/opt/gitlab/postgresql gitlabhq_production -f %FILE.sql%
```
