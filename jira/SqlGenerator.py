# Copyright (c) 2017-present OW2 http://www.ow2.org
#
# Permission is hereby granted, free of charge, to any person obtaining a copy
# of this software and associated documentation files (the "Software"), to deal
# in the Software without restriction, including without limitation the rights
# to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
# copies of the Software, and to permit persons to whom the Software is
# furnished to do so, subject to the following conditions:
#
# The above copyright notice and this permission notice shall be included in
# all copies or substantial portions of the Software.
#
# THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
# IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
# FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
# AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
# LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
# OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
# THE SOFTWARE.


# WARNING : please verify that the gitlab project has its notifications disabled
# coding: utf8

import pprint
from configparser import ConfigParser, ExtendedInterpolation
import requests
from requests.auth import HTTPBasicAuth
import re
from StringIO import StringIO
#import scp
from Utils import getJiraUser, getJiraRoles, getJiraVersions, addUsersToGroup, convertUsernameToGitlab, addMilestoneToGitlab, convertUsernameToGitlab
from Utils import addUserToGitlab
from Utils import whatisthis
import io
import unicodedata
import random

from requests.packages.urllib3.exceptions import InsecureRequestWarning
requests.packages.urllib3.disable_warnings(InsecureRequestWarning)

Config = ConfigParser(interpolation=ExtendedInterpolation())
Config.read("./config.cfg")

jiraUrl = Config.get('JIRA', 'url')
jiraAccount = eval(Config.get('JIRA', 'account'))
jiraProject = Config.get('JIRA', 'project')
# jiraGroup = Config.get('JIRA', 'group') # unused/deprecated
jiraHeader = eval(Config.get('JIRA', 'header'))
gitLabUrl = Config.get('GitLab', 'url')
gitLabToken = Config.get('GitLab', 'token')
gitLabProject = Config.get('GitLab', 'project')
gitLabProjectId = eval(Config.get('GitLab', 'projectId'))
gitLabVersion = Config.get('GitLab', 'version')
gitLabApiUrl = Config.get('GitLab', 'api')
verifySslCertificate = Config.getboolean('Global', 'verifySslCertificate')

SQL_File = io.open(jiraProject.lower() + '-issues.sql', 'w', encoding="utf-8")


def escapeSQL(body):
    r = u""
    if body:
        try:
            r = body  # .decode('iso-8859-1')
        except UnicodeEncodeError as e:
            whatisthis(body)
            whatisthis(r)
            raise
    return r.replace(u'\'', u'\'\'')


reservedNames = ['admin']


def translateUsername(jiraName):
    gitlabName = escapeSQL(jiraName)
    if gitlabName in reservedNames:
        gitlabName = 'Jira' + gitlabName
    return gitlabName


def addAttachment(attachment, iid, project_id, gitlabReporter):
    # print str(attachment)
    if 'author' in attachment and hasattr(attachment['author'], '__iter__') and 'name' in attachment['author']:
        author = escapeSQL(attachment['author']['name'])
    else:
        # print 'No author found. Setting author to "Undefined" user.'
        print "No attachment author found. Setting attachment author to issue's reporter"
        # not ideal at all but JIRA doesn't return the attachment author username when the
        # user account is deleted...
        author = gitlabReporter

    # author = unicodedata.normalize('NFKD', unicode(author)).encode('ascii', 'ignore')
    # if ' ' in author:
    #   author = author.replace(' ', '_')
    # if '@' in author:
    #   author = author.split('@')[0]

    _file = requests.get(
        attachment['content'],
        auth=HTTPBasicAuth(*jiraAccount),
        verify=verifySslCertificate,
    )
    if _file.status_code == 500:
        print attachment['content'] + ' does not exist anymore.'
        return
    filename = unicodedata.normalize(
        'NFKD', attachment['filename']).encode('ascii', 'ignore')
    _content = StringIO(_file.content)
    # uploading attachment
    # for this to work using SUDO, the project should have the following permission settings:
    # - At least Project Visibility : internal
    # - Issues :  Everyone with access
    # OR in a private project, users should be explicitely granted in the permissions page
    # Ie, inheritence from group is not enough
    file_info = requests.post(
        gitLabUrl + 'api/' + gitLabVersion + '/projects/%s/uploads' % gitLabProjectId,
        headers={
            'PRIVATE-TOKEN': gitLabToken,
            # GITLAB_USER_NAMES.get(author, author)
            'SUDO': convertUsernameToGitlab(author)
        },
        files={
            'file': (
                filename,
                _content
            )
        },
        verify=verifySslCertificate
    )
    # print str(file_info)
    if 'markdown' in file_info.json():
        # now we got the upload URL. Let's post the comment with an
        # attachment
        SQL_File.write(u'INSERT INTO notes (note, noteable_type, author_id, created_at, updated_at, project_id, noteable_id) SELECT \'{note}\', \'Issue\', (SELECT id FROM users WHERE username = \'{username}\'), \'{created_at}\', \'{updated_at}\', {project_id}, (SELECT id FROM issues WHERE iid = {iid} AND project_id = {project_id});\n'.format(
            note=file_info.json()['markdown'], created_at=attachment['created'], updated_at=attachment['created'], username=author, iid=iid, project_id=project_id))
    else:
        # print 'markdown not in file_info'
        print file_info.text
        raise Exception("'markdown' not found")
    print attachment['filename'].encode('utf-8'), filename
    del _content


def processIssue(issue):
        # print str(issue)
    if 'fields' not in issue:
        print issue
        raise Exception
    if 'reporter' in issue['fields'] and hasattr(issue['fields']['reporter'], '__iter__') and 'name' in issue['fields']['reporter']:
        jiraReporter = escapeSQL(issue['fields']['reporter']['name'])
    else:
        # if not found in search issue's result fields, we gonna look into issue itself
        # This case happen when the user account is deleted from JIRA
        issue_info = requests.get(
            jiraUrl + 'rest/api/2/issue/%s/?fields=reporter' % issue['id'],
            auth=HTTPBasicAuth(*jiraAccount),
            verify=verifySslCertificate,
            headers=jiraHeader
        ).json()

        if issue_info['fields']['reporter']['name']:
            jiraReporter = issue_info['fields']['reporter']['name']
        else:
            print 'No reporter found. Setting reporter to "Undefined" user.'
            jiraReporter = "Undefined"

    description = escapeSQL(issue['fields']['description'])
    # TODO
    # References to other issues are jiraUrl + '/browse/' + jiraProject + '-'
    title = escapeSQL(issue['fields']['summary'])
    iid = issue['key'].split(u'-')[1]
    issueState = issue['fields']['status']['name'].lower()
    if 'clos' in issueState:
        state = 'closed'
    else:
        state = 'opened'
    #jiraReporter = unicodedata.normalize('NFKD', unicode(jiraReporter)).encode('ascii', 'ignore')
    gitlabReporter = addUserToGitlab(jiraReporter, gitlabUsers, gitLabApiUrl, gitLabToken,
                                     gitLabProjectId, jiraUsers, jiraUrl, jiraAccount, jiraHeader, verifySslCertificate)
    # reporter = gitlabReporter
    # if '@' in reporter:
    #   reporter = reporter.split('@')[0]
    # if ' ' in reporter:
    #   reporter = reporter.replace(' ', '_')
    if 'fixVersions' in issue['fields']:
        if len(issue['fields']['fixVersions']) > 0:
            milestone_name = issue['fields']['fixVersions'][0]['name']
        else:
            milestone_name = ''
    else:
        milestone_name = ''
    try:
        #SQL = u'INSERT INTO issues (title, project_id, iid, author_id, state, created_at, updated_at, description) SELECT \'{title}\', {project_id}, {iid}, id, \'{state}\', \'{created_at}\', \'{updated_at}\', \'{description}\' FROM users WHERE username = \'{username}\';\n'.format(title = title, iid = iid, project_id = gitLabProjectId, state = state, created_at = issue['fields']['created'], updated_at = issue['fields']['updated'], username = gitlabReporter, description = description)
        SQL = u'INSERT INTO issues (title, project_id, iid, author_id, state, created_at, updated_at, description, milestone_id) SELECT \'{title}\', {project_id}, {iid}, id, \'{state}\', \'{created_at}\', \'{updated_at}\', \'{description}\', (SELECT id from milestones WHERE project_id={project_id} and title=\'{milestone_name}\') FROM users WHERE username = \'{username}\';\n'.format(
            title=title, iid=iid, project_id=gitLabProjectId, state=state, created_at=issue['fields']['created'], updated_at=issue['fields']['updated'], username=gitlabReporter, description=description, milestone_name=milestone_name)
    except UnicodeEncodeError as e:
        print title
        print iid
        print gitlabReporter
        print description.encode('utf8', 'ignore')
        raise
    except UnicodeDecodeError as e:
        print title
        print iid
        print gitlabReporter
        print description.encode('utf8', 'ignore')
        raise
    SQL_File.write(SQL)

    # linking components to issues
    # compoents are previously added from importComponentsAsLabels()
    if 'components' in issue['fields']:
        for component in issue['fields']['components']:
            SQL_File.write(u'INSERT INTO label_links (label_id, target_id, target_type) (SELECT l.id, i.id, \'Issue\' FROM labels l, issues i WHERE l.title = \'{label}\' AND iid = {iid} AND l.project_id = {project_id} AND i.project_id = {project_id});\n'.format(
                label=component['name'], iid=iid, project_id=gitLabProjectId))

    # get comments and attachments
    issue_info = requests.get(
        jiraUrl +
        'rest/api/2/issue/%s/?fields=attachment,comment' % issue['id'],
        auth=HTTPBasicAuth(*jiraAccount),
        verify=verifySslCertificate,
        headers=jiraHeader
    ).json()

    for comment in issue_info['fields']['comment']['comments']:
        author = comment['author']['name']
        #author = unicodedata.normalize('NFKD', unicode(author)).encode('ascii', 'ignore')
        body = escapeSQL(comment['body'])
        gitlabAuthor = addUserToGitlab(author, gitlabUsers, gitLabApiUrl, gitLabToken,
                                       gitLabProjectId, jiraUsers, jiraUrl, jiraAccount, jiraHeader, verifySslCertificate)
        author = gitlabAuthor
    # author value already returned by addUserToGitlab()
    #   if ' ' in author:
    #     author = author.replace(' ', '_')
    #   if '@' in author:
    #     author = author.split('@')[0]
        SQL_File.write(u'INSERT INTO notes (note, noteable_type, author_id, created_at, updated_at, project_id, noteable_id) SELECT \'{note}\', \'Issue\', (SELECT id FROM users WHERE username = \'{username}\'), \'{created_at}\', \'{updated_at}\', {project_id}, (SELECT id FROM issues WHERE iid = {iid} AND project_id = {project_id});\n'.format(
            note=body, project_id=gitLabProjectId, created_at=comment['created'], updated_at=comment['updated'], username=author, iid=iid))

    if len(issue_info['fields']['attachment']):
        print "Attachment found"
        for attachment in issue_info['fields']['attachment']:
            # print gitLabProjectId
            addAttachment(attachment, iid, gitLabProjectId, gitlabReporter)

    print "Issue #" + str(iid) + " processed."


def importComponentsAsLabels():
    SQL_File.write(u'DELETE FROM labels WHERE project_id = {project_id};\n'.format(
        project_id=gitLabProjectId))
    jira_project = requests.get(jiraUrl + 'rest/api/2/project/' + jiraProject,
                                auth=HTTPBasicAuth(*jiraAccount),
                                headers=jiraHeader)
    components = jira_project.json()['components']
    for component in components:
        name = escapeSQL(component['name'])
        if 'description' in component:
            description = escapeSQL(component['description'])
        else:
            description = ""

        def r(): return random.randint(0, 255)
        SQL_File.write(u'INSERT INTO labels (title, project_id, description, type, color) VALUES (\'{title}\', {project_id}, \'{description}\', \'ProjectLabel\', \'{color}\');\n'.format(
            title=name, project_id=gitLabProjectId, description=description, color='#%02X%02X%02X' % (r(), r(), r())))


def red(text):
    return('\x1b[0;30;41m' + text + '\x1b[0m')

######## MAIN PROGRAM ##########
print 'Starting JIRA to Gitlab migrator...'

# we will always use input() regardless of python version
try:
    input = raw_input
except NameError:
    pass

print('Source JIRA URL and Project: {} / {}'.format(jiraUrl, jiraProject))
print('Destination GitLab URL and Project: {} / {}'.format(gitLabUrl, gitLabProject))

print(red("Warning!") + " You should disable email notifications in GitLab to prevent \
any disaster. Press CTRL+C to exit or Enter to continue " + red("Warning!"))

input('?')

# Project and group creation if not exists

if not gitLabProject:
    createGroup = requests.post(
        gitLabUrl + 'api/' + gitLabVersion + '/groups',
        headers={
            'PRIVATE-TOKEN': gitLabToken,
        },
        data={
            'name': jiraProject.lower(),
            'path': jiraProject.lower()
        },
        verify=verifySslCertificate
    )
    print createGroup.text
    createProject = requests.post(
        gitLabUrl + 'api/' + gitLabVersion + '/projects',
        headers={
            'PRIVATE-TOKEN': gitLabToken,
        },
        data={
            'path': jiraProject.lower()
        },
        verify=verifySslCertificate
    )
    print createProject.text
    gitLabProjectId = createProject.json()['id']

    transfertProjectToGroup = requests.post(
        gitLabUrl + 'api/' + gitLabVersion + '/groups/' +
        str(createGroup.json()['id']) + '/projects/' +
        str(createProject.json()['id']),
        headers={
            'PRIVATE-TOKEN': gitLabToken,
        },
        data={
            'name': jiraProject.lower(),
            'path': jiraProject.lower()
        },
        verify=verifySslCertificate
    )
    gitLabProject = jiraProject.lower() + "/" + jiraProject.lower()
    print transfertProjectToGroup.text

    notifSetting = requests.put(
        gitLabUrl + 'api/' + gitLabVersion + '/projects/' +
        str(createProject.json()['id']) + '/notification_settings',
        headers={
            'PRIVATE-TOKEN': gitLabToken,
        },
        data={
            'level': 'disabled'},
        verify=verifySslCertificate
    )
    print notifSetting.text


if not gitLabProjectId:
    #    find out the ID of the project.
    try:
        projects = requests.get(
            gitLabUrl + 'api/' + gitLabVersion + '/projects',
            headers={'PRIVATE-TOKEN': gitLabToken},
        )
        projects.raise_for_status()

        for project in projects.json():
            if project['path_with_namespace'] == gitLabProject:
                gitLabProjectId = project['id']
                gitLabGroup = project['namespace']['path']
                break

    except requests.exceptions.HTTPError as e:
        print e
        raise
    # else:
    #    raise Exception("Gitlab says: Not authorized (401)")

if not gitLabProjectId:
    raise Exception("Unable to find %s in gitlab!" % gitLabProject)

# at this point we got everything to work on projects


jiraVersions = getJiraVersions(
    jiraProject, jiraUrl, jiraAccount, jiraHeader, verifySslCertificate)
for jiraVersion in jiraVersions:
    # element is dict
    gitlabMilestone = (
        jiraVersion['name'],
        jiraVersion['description'] if 'description' in jiraVersion else False,
        jiraVersion['releaseDate'] if 'releaseDate' in jiraVersion else False
    )
    try:
        addMilestoneToGitlab(gitlabMilestone, gitLabApiUrl,
                             gitLabToken, gitLabProjectId, verifySslCertificate)
    except Exception as e:
        print e
# exit()

jiraRoles = getJiraRoles(jiraProject, jiraUrl, jiraAccount,
                         jiraHeader, verifySslCertificate)
# jiraUsers is set once with inital users data from JIRA roles
# it will updated later during the issues content migration

# extract deduplicated list of users in the project's role
usersList = []
for role, users in jiraRoles.items():
    # print role
    usersList.extend(users)

usersList = set(usersList)

gitlabLevelUsers = {}
gitlabLevelUsers[50] = []
gitlabLevelUsers[30] = []
gitlabLevelUsers[10] = []
jiraUsers = {}
gitlabUsers = {}
for jiraUser in usersList:
    #if jiraUser == "mh": import pdb; pdb.set_trace()
    jiraUsers.update(getJiraUser(jiraUser, jiraUrl,
                                 jiraAccount, jiraHeader, verifySslCertificate))
    if jiraUser in jiraRoles['Administrators']:
        # level 50
        print "user %s is level 50" % jiraUser
        gitlabLevelUsers[50].append(convertUsernameToGitlab(jiraUser))
        continue
    elif jiraUser in jiraRoles['Developers']:
        # level 30
        print "user %s is level 30" % jiraUser
        gitlabLevelUsers[30].append(convertUsernameToGitlab(jiraUser))
        continue
    else:
        print "user %s is level 10" % jiraUser
        gitlabLevelUsers[10].append(convertUsernameToGitlab(jiraUser))
        # level 10

for jiraUser in usersList:
    if jiraUser in jiraUsers:
            #jiraUser = unicodedata.normalize('NFKD', unicode(jiraUser)).encode('ascii', 'ignore')
        addedUsername = addUserToGitlab(jiraUser, gitlabUsers, gitLabApiUrl, gitLabToken,
                                        gitLabProjectId, jiraUsers, jiraUrl, jiraAccount, jiraHeader, verifySslCertificate)


for level, usernames in gitlabLevelUsers.items():
    # print level
    # print usernames
    addUsersToGroup(usernames, level, gitlabUsers, jiraUsers,
                    gitLabApiUrl, gitLabToken, gitLabGroup, verifySslCertificate)
# pour chaque u
# si u est admin : => 50
# si u est dev => 30
# si u est ..


# print jiraUsers

# roles in JIRA are
# Administrators
# Developers
# Users
# in gitlab :
# 10 => Guest access
# 20 => Reporter access
# 30 => Developer access
# 40 => Master access
# 50 => Owner access # Only valid for groups


importComponentsAsLabels()

acquiredIssues = 0
counter = 0
MAX = 10000
while True:
    jira_issues = requests.get(
        jiraUrl +
        'rest/api/2/search?jql=project=%s+&startAt=%d' % (
            jiraProject, acquiredIssues),
        auth=HTTPBasicAuth(*jiraAccount),
        verify=verifySslCertificate,
        headers=jiraHeader
    ).json()

    #pp = pprint.PrettyPrinter()
    #pp.pprint(jira_issues)
    acquiredIssues += jira_issues['maxResults']
    print "Starting at: %d" % jira_issues['startAt']
    print "Max results: %d" % jira_issues['maxResults']
    print "Total issues: %d" % jira_issues['total']

    for issue in jira_issues['issues']:
        counter = counter + 1
        print "Processing issue number: %d" % counter
        if counter < MAX:
            processIssue(issue)
    if acquiredIssues >= jira_issues['total']:
        print "**************************************"
        acquiredIssues = jira_issues['total']
        break


# Replace all the "{code}" occurrences by "```"
SQL_File.write(
    u'UPDATE issues SET description = regexp_replace(description, \'{{code[^}}]*}}(.*?){{code}}\', \'\n```\\1```\', \'g\') WHERE project_id = {project_id};\n'.format(project_id=gitLabProjectId))
SQL_File.write(
    u'UPDATE notes SET note = regexp_replace(note, \'{{code[^}}]*}}(.*?){{code}}\', \'```\n\\1```\', \'g\'), note_html = NULL WHERE noteable_type = \'Issue\' AND noteable_id IN (SELECT id FROM issues WHERE project_id = {project_id});\n'.format(project_id=gitLabProjectId))
# Replace all references to other issues such as "PROJECT-ISSUE_ID" by "#ISSUE_ID"
SQL_File.write(u'UPDATE issues SET description = regexp_replace(description, \'{project_name}-(\\d*)\', \'#\\1\', \'g\') WHERE project_id = {project_id};\n'.format(
    project_id=gitLabProjectId, project_name=jiraProject))
SQL_File.write(u'UPDATE notes SET note = regexp_replace(note, \'{project_name}-(\\d*)\', \'#\\1\', \'g\'), note_html = NULL WHERE noteable_type = \'Issue\' AND noteable_id IN (SELECT id FROM issues WHERE project_id = {project_id});\n'.format(
    project_id=gitLabProjectId, project_name=jiraProject))
# Replace all username mentions
SQL_File.write(
    u'UPDATE issues SET description = regexp_replace(description, \'\\[~([^\\]]*)\\]\', \'@\\1\', \'g\') WHERE project_id = {project_id};\n'.format(project_id=gitLabProjectId))
SQL_File.write(
    u'UPDATE notes SET note = regexp_replace(note, \'\\[~([^\\]]*)\\]\', \'@\\1\', \'g\'), note_html = NULL WHERE noteable_type = \'Issue\' AND noteable_id IN (SELECT id FROM issues WHERE project_id = {project_id});\n'.format(project_id=gitLabProjectId))

print "Generated SQL code for %s issues from project %s" % (acquiredIssues, jiraProject)
