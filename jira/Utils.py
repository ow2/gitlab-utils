#Copyright (c) 2017-present OW2 http://www.ow2.org
#
#Permission is hereby granted, free of charge, to any person obtaining a copy
#of this software and associated documentation files (the "Software"), to deal
#in the Software without restriction, including without limitation the rights
#to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
#copies of the Software, and to permit persons to whom the Software is
#furnished to do so, subject to the following conditions:
#
#The above copyright notice and this permission notice shall be included in
#all copies or substantial portions of the Software.
#
#THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
#IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
#FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
#AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
#LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
#OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
#THE SOFTWARE.


#coding: utf8
import pprint
import requests
import unicodedata
from requests.auth import HTTPBasicAuth

def whatisthis(s):
  if isinstance(s, str):
    print 'ascii string'
  elif isinstance(s, unicode):
    print 'unicode string'
  else:
    print 'not a string'

def IsUnicodeString(s):
    if isinstance(s, basestring):
        if isinstance(s, unicode):
            return True
        elif isinstance(s, str):
            return s.decode()
    else:
        return False

def getJiraUser(username, JIRA_URL, JIRA_ACCOUNT, JIRA_HEADER, VERIFY_SSL_CERTIFICATE):
    u = {}
    fUsers = True

    jira_user = requests.get(
        JIRA_URL + 'rest/api/2/user?username=%s' % (username),
        auth=HTTPBasicAuth(*JIRA_ACCOUNT),
        verify=VERIFY_SSL_CERTIFICATE,
        headers=JIRA_HEADER
    )
    # print jira_users.text
    userDetail = jira_user.json()
    #print str(userDetail)
    if not 'errorMessages' in userDetail:
        u[userDetail['name']] = userDetail
        # print userDetail['name']
        print "user '%s' found" % userDetail['name']
        return u
    else:
        return False


def getJiraRoles(JIRA_PROJECT, JIRA_URL, JIRA_ACCOUNT, JIRA_HEADER, VERIFY_SSL_CERTIFICATE):
    # returns a dictionary with for each roles names as key, a list of members
    pp = pprint.PrettyPrinter()
    pRoles = requests.get(
        JIRA_URL + 'rest/api/2/project/%s/role' % (JIRA_PROJECT),
        auth=HTTPBasicAuth(*JIRA_ACCOUNT),
        verify=VERIFY_SSL_CERTIFICATE,
        headers=JIRA_HEADER
    ).json()

    #print pRoles

    if not 'errorMessages' in pRoles:
        #print str(pRoles)

        pRolesDetails = {}

        for role in pRoles.keys():
            pRolesDetails[role] = requests.get(
                pRoles[role],
                auth=HTTPBasicAuth(*JIRA_ACCOUNT),
                verify=VERIFY_SSL_CERTIFICATE,
                headers=JIRA_HEADER
            ).json()

            #pp.pprint(pRolesDetails)
            roleMembership = []
            if not 'errorMessages' in pRolesDetails[role]:
                for member in pRolesDetails[role]['actors']:
                    if member['type'] == 'atlassian-user-role-actor':
                        roleMembership.append(member['name'])

                pRoles[role] = roleMembership


        return pRoles
            #         print "role %s => %s" % (pRolesDetails[role].key, str(members))
            #

def addUsersToGroup(usernames, level, gitlabUsers, jiraUsers, GITLAB_API_URL, GITLAB_TOKEN, GITLAB_PROJECT_ID, VERIFY_SSL_CERTIFICATE):
    # takes a list of usernames and the level
    # TODO we might want to look in gitlabUsers for the given username to get the gitlab user ID
    pp = pprint.PrettyPrinter()

    for username in usernames:
        #pp.pprint(gitlabUsers)
        print "adding user %s (uid %s / level %s) to group %s" % (username, gitlabUsers[username]['id'], level, GITLAB_PROJECT_ID)
        addUserToGroup = requests.post(
            GITLAB_API_URL + '/groups/%s/members' % GITLAB_PROJECT_ID,
            headers={
                   'PRIVATE-TOKEN': GITLAB_TOKEN,
                   },
            verify=VERIFY_SSL_CERTIFICATE,
            data={
                'user_id': gitlabUsers[username]['id'],
                'access_level': level
            })

        if addUserToGroup.encoding is None:
            addUserToGroup.encoding = 'utf-8'
        try:
             if addUserToGroup.status_code not in [201, 409]:
               print addUserToGroup.url
               print addUserToGroup.text.encode('utf-8')
               #print jiraUsers[name]['emailAddress']
               raise Exception ("User %s not added" % jsonGitUser['name'])
        except:
            import pdb; pdb.set_trace()

def getJiraVersions(JIRA_PROJECT, JIRA_URL, JIRA_ACCOUNT, JIRA_HEADER, VERIFY_SSL_CERTIFICATE):
    # values
    # 0
    # self	"https://jira.ow2.org/rest/api/2/version/10070"
    # id	"10070"
    # description	"Release Candidate 1 for 1.0 version"
    # name	"1.0-rc1"
    # archived	false
    # released	true
    # releaseDate	"2010-03-23"
    # userReleaseDate	"23/Mar/10"
    # projectId	10030

    # in gitlab
    # id (required) - The ID or URL-encoded path of the project owned by the authenticated user
    # title (required) - The title of an milestone
    # description (optional) - The description of the milestone
    # due_date (optional) - The due date of the milestone
    # start_date (optional) - The start date of the milestone

    # https://jira.ow2.org/rest/api/2/project/LEMONLDAP/version
    pp = pprint.PrettyPrinter()
    pVersions = requests.get(
        JIRA_URL + 'rest/api/2/project/%s/version?maxResults=500' % (JIRA_PROJECT),
        auth=HTTPBasicAuth(*JIRA_ACCOUNT),
        verify=VERIFY_SSL_CERTIFICATE,
        headers=JIRA_HEADER
    ).json()

    #print pRoles
    #pp.pprint(pVersions)
    if not 'errorMessages' in pVersions:
        return pVersions['values']
        #pp.pprint(pVersions)

def addMilestoneToGitlab(milestone, GITLAB_API_URL, GITLAB_TOKEN, GITLAB_PROJECT_ID, VERIFY_SSL_CERTIFICATE):
    # expect tuple as milestone
    title, description, due_date = milestone

    print "adding milestone %s to project %s" % (title, GITLAB_PROJECT_ID)
    addMilestone = requests.post(
        GITLAB_API_URL + '/projects/%s/milestones' % GITLAB_PROJECT_ID,
        headers={
               'PRIVATE-TOKEN': GITLAB_TOKEN,
               },
        verify=VERIFY_SSL_CERTIFICATE,
        data={
            'title': title,
            'description': description,
            'due_date': due_date
        })

    if addMilestone.encoding is None:
        addMilestone.encoding = 'utf-8'

    if addMilestone.status_code not in [201, 409]:
        print addMilestone.status_code
        print addMilestone.url
        print addMilestone.text.encode('utf-8')
        #print jiraUsers[name]['emailAddress']
        raise Exception ("Milestone %s not added" % title)


def convertUsernameToGitlab(jiraUsername):
    # convert username to gitlab username, stripping unwanted chars
    # return the username if altered, Else return False
    gitlabUsername = jiraUsername
    if ' ' in jiraUsername:
        print "there is space in username %s" % jiraUsername
        gitlabUsername = jiraUsername.replace(' ', '_')

    if '@' in jiraUsername:
        print "there is '@' in username %s" % jiraUsername
    #    domain = name.split('@')[1]
        gitlabUsername = jiraUsername.split('@')[0]

    # if not IsUnicodeString(name):
    #     gitlabUsername = unicode(gitlabUsername))

    gitlabUsername = unicodedata.normalize('NFD', gitlabUsername).encode('ascii', 'ignore')

    return gitlabUsername if jiraUsername != gitlabUsername else jiraUsername


def addUserToGitlab(name, gitlabUsers, GITLAB_API_URL, GITLAB_TOKEN, GITLAB_PROJECT_ID, jiraUsers, JIRA_URL, JIRA_ACCOUNT, JIRA_HEADER, VERIFY_SSL_CERTIFICATE):
    print "*** adding user %s in gitlab ..." % name
    realName = name
    domain = 'gitlabmigration'

    # if ' ' in name:
    #     print "there is space in username %s" % name
    #     name = name.replace(' ', '_')
    #
    # if '@' in name:
    #     print "there is '@' in username %s" % name
    # #    domain = name.split('@')[1]
    #     name = name.split('@')[0]

    name = convertUsernameToGitlab(name)

    nameIsSpecial = True if realName != name else False

    # We add to local jiraUsers dict with that user's JIRA details
    if realName not in jiraUsers:
        user = getJiraUser(realName, JIRA_URL, JIRA_ACCOUNT, JIRA_HEADER, VERIFY_SSL_CERTIFICATE)
        if user:
            print "updating local jiraUsers with %s" % realName
            # print str(user)
            jiraUsers.update(user)
        else:
            # user details are not found
            # but remember an object still references that user in JIRA
            # we should add a corresponding dummy user in gitlab
            jiraUsers.update({unicode(realName): { u'dummy': True }})
        print str(jiraUsers)

    #We're looking for an existing user
    #We could use a cache here
    resGitUsers = requests.get(
        GITLAB_API_URL + '/users?username=%s' % name,
        headers={
              'PRIVATE-TOKEN': GITLAB_TOKEN,
              },
        verify=VERIFY_SSL_CERTIFICATE
        )
    jsonGitUser = resGitUsers.json()

    # Make sure the user is NOT found in GitLab
    if not jsonGitUser:
    # is he a known JIRA user ?
      if realName in jiraUsers:
          # is he dummy ?
          if 'dummy' not in jiraUsers[realName]:
            print u'Adding user %s in Gitlab (found in JIRA)' % name
            addUser = requests.post(
                GITLAB_API_URL + '/users',
                headers={
                      'PRIVATE-TOKEN': GITLAB_TOKEN,
                      },
                verify=VERIFY_SSL_CERTIFICATE,
                data={
                    # we use the processed 'name' as username but realName for extern_uid
                    'email': jiraUsers[realName]['emailAddress'],
                    #'email': name + '@' + domain,
                    'username': name,
                    'name': jiraUsers[realName]['displayName'],
                    'provider': 'ldapmain',
                    #'password': name+'p4ssWd',
                    'extern_uid': 'uid=' + (realName if nameIsSpecial else name) + ',ou=people,dc=ow2,dc=org',
                    'skip_confirmation': True,
                    'reset_password': True
                }
            )
        #Unknown JIRA user for the comment/issue : the account was deleted. We'll create a Gitlab one with curious credentials
          else:
            print 'Add dummy user (account deleted in JIRA but references an object) ' + name
            addUser = requests.post(
                GITLAB_API_URL + '/users',
                headers={
                      'PRIVATE-TOKEN': GITLAB_TOKEN,
                      },
                verify=VERIFY_SSL_CERTIFICATE,
                data={
                    'email': name + '@' + domain,
                    'username': name,
                    'name': realName,
                    # TODO generate a hard password OR add a dummy LDAP identity
                    #'password': name+'p4ssWd',
                    'provider': 'ldapmain',
                    'extern_uid': 'uid=' + name + ',ou=people,dc=ow2,dc=org',
                    'skip_confirmation': True,
                    'reset_password': True
                }
            )

      if addUser.status_code != 201:
        # pp = pprint.PrettyPrinter()
        # pp.pprint(jiraUsers.keys())
        print "mail:" + jiraUsers[realName]['emailAddress']
        print addUser.text.encode('utf-8')
    #We check that the new user is saved
      resGitUsers = requests.get(
        GITLAB_API_URL + '/users?username=%s' % name,
        headers={
          'PRIVATE-TOKEN': GITLAB_TOKEN,
          },
        verify=VERIFY_SSL_CERTIFICATE
        )
      if len(resGitUsers.json())==0:
        raise Exception("Unable to find user %s in gitlab" % name)
      jsonGitUser = resGitUsers.json()
      #Some Gitlab versions put a requested user in a list. Some not, so if we get a list, we take the first element.
      if isinstance(jsonGitUser, list):
        jsonGitUser = jsonGitUser[0]
    else:
    #the user is known in Gitlab, but we stll have to verify, especially if the account is associated with matching the LDAP DN
      #Some Gitlab versions put a requested user in a list. Some not, so if we get a list, we take the first element.
        if isinstance(jsonGitUser, list):
          jsonGitUser = jsonGitUser[0]
        if name in jiraUsers:
          #if (jsonGitUser['name'] != jiraUsers[name]['displayName'] or jsonGitUser['email'] != jiraUsers[name]['emailAddress']):

            print "user exists in Gitlab : verifying user data: " + name
            #print jsonGitUser.keys()
            #print jiraUsers[name].keys()
            #print str(jsonGitUser)

            #print str(jiraUsers[name])

            if jsonGitUser['name'] != (jiraUsers[name]['displayName'] if 'dummy' not in jiraUsers[name] else name) :
            #  or (jsonGitUser['email'] != jiraUsers[name]['emailAddress']):
                print "Name for user %s doesn't match : updating it" % name
                modGitUser = requests.put(
                GITLAB_API_URL + '/users/%s' % jsonGitUser['id'],
                headers={
                    'PRIVATE-TOKEN': GITLAB_TOKEN,
                    },
                verify=VERIFY_SSL_CERTIFICATE,
                data={
                    'name': jiraUsers[name]['displayName'] if 'dummy' not in jiraUsers[name] else name,
                    'email': jiraUsers[name]['emailAddress']
                })

            has_ldap_identity=False
            if jsonGitUser['identities']:
                for identity in jsonGitUser['identities']:
                    if identity['provider'] == 'ldapmain':
                        has_ldap_identity=True
                        break

            if not has_ldap_identity:
                # no ldap identity : we add it
                print "user %s has no LDAP identity: adding it" % name
                modGitUser = requests.put(
                GITLAB_API_URL + '/users/%s' % jsonGitUser['id'],
                headers={
                    'PRIVATE-TOKEN': GITLAB_TOKEN,
                    },
                verify=VERIFY_SSL_CERTIFICATE,
                data={
                    #'name': jiraUsers[name]['displayName'],
                    'extern_uid': 'uid=' + name + ',ou=people,dc=ow2,dc=org',
                    'provider': 'ldapmain',
                    'skip_confirmation': True
                })
                #print "modGitUser.text" + modGitUser.text
            else:
                # there is an ldap identity, we finish here as we don't want to overwrite the LDAP DN of the user
                print "user %s already has an LDAP identity: next" % name


        #            raise Exception("User mismatch %s %s") % (jsonGitUser[0]['name'], jiraUsers[reporter]['displayName'])
        #    print resGitUsers.text
    # addUserToProject = requests.post(
    #     GITLAB_API_URL + '/projects/%s/members' % GITLAB_PROJECT_ID,
    #     headers={
    #            'PRIVATE-TOKEN': GITLAB_TOKEN,
    #            },
    #     verify=VERIFY_SSL_CERTIFICATE,
    #     data={
    #         'user_id': jsonGitUser['id'],
    #         'access_level': 10
    #     })
    # if addUserToProject.status_code not in [201, 409]:
    #   print addUserToProject.url
    #   print addUserToProject.text
    #   print jiraUsers[name]['emailAddress']
    #   raise Exception ("User %s not added" % jsonGitUser['name'])
    #removeNotifications = requests.put(
        #GITLAB_URL + 'api/' + GITLAB_VERSION + '/notification_settings?level=disabled',
        #headers={
               #'PRIVATE-TOKEN': GITLAB_TOKEN,
               #'SUDO': name,
               #},
        #verify=VERIFY_SSL_CERTIFICATE,
        #data={
            #'user_id': jsonGitUser['id'],
        #})
    #if removeNotifications.status_code not in [200]:
      #print removeNotifications.status_code
      #print removeNotifications.url
      #print removeNotifications.text
      #print jiraUsers[name]['emailAddress']
      #raise Exception ("User %s notification level couldn't be changed." % jsonGitUser['name'])
    gitlabUsers[name] = jsonGitUser
    if nameIsSpecial:
        return name
    else:
        return realName

def changeUsersLevel(name, access_level):
    editProjectUser = requests.put(
        GITLAB_API_URL + '/projects/{0}/members/{1}'.format(GITLAB_PROJECT_ID, gitlabUsers[name]['id']),
        headers={
               'PRIVATE-TOKEN': GITLAB_TOKEN,
               },
        verify=VERIFY_SSL_CERTIFICATE,
        data={
            'access_level': access_level
        })
    if editProjectUser.status_code not in [200]:
      print editProjectUser.status_code
      print editProjectUser.text
      raise Exception ("User %s not edited" % name)
