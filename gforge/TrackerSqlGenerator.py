#Copyright (c) 2017-present OW2 http://www.ow2.org
#
#Permission is hereby granted, free of charge, to any person obtaining a copy
#of this software and associated documentation files (the "Software"), to deal
#in the Software without restriction, including without limitation the rights
#to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
#copies of the Software, and to permit persons to whom the Software is
#furnished to do so, subject to the following conditions:
#
#The above copyright notice and this permission notice shall be included in
#all copies or substantial portions of the Software.
#
#THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
#IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
#FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
#AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
#LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
#OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
#THE SOFTWARE.

#coding: utf8
from configparser import ConfigParser, ExtendedInterpolation
import xml.etree.ElementTree as ET
import requests
from requests.auth import HTTPBasicAuth
import re
from StringIO import StringIO
import io
from datetime import datetime
import base64
import random

Config = ConfigParser(interpolation = ExtendedInterpolation())
Config.read("./config.cfg")

gforgeProject = Config.get('GForge', 'project')
gitLabUrl = Config.get('GitLab', 'url')
gitLabToken = Config.get('GitLab', 'token')
gitLabProject = Config.get('GitLab', 'project')
gitLabVersion = Config.get('GitLab', 'version')
gitLabApiUrl = Config.get('GitLab', 'api')
verifySslCertificate = Config.getboolean('Global', 'verifySslCertificate')

SQL_File = io.open(gforgeProject.lower() + '-issues.sql', 'w', encoding="utf-8")
# IMPORTANT !!!
# make sure that user (in gitlab) has access to the project you are trying to
# import into. Otherwise the API request will fail.

# jira user name as key, gitlab as value
# if you want dates and times to be correct, make sure every user is (temporarily) admin

def importCategoriesAsLabels():
  SQL_File.write(u'DELETE FROM labels WHERE project_id = {project_id};\n'.format(project_id = gitLabProjectId));
  categories = getCategories()
  labels = []
  categoryFilter = Config.get('GForge', 'categories')
  for category in categories:
    #category = Config.get('ASM', category)
    description = ""
    r = lambda : random.randint(0,255)
    if category in categoryFilter and category != 'None' and category not in labels:
      SQL_File.write(u'INSERT INTO labels (title, project_id, description, type, color) VALUES (\'{title}\', {project_id}, \'{description}\', \'ProjectLabel\', \'{color}\');\n'.format(title = category, project_id = gitLabProjectId, description = description, color = '#%02X%02X%02X' % (r(), r(), r())))
      labels.append(category)

def getUsers():
  known_fields = ['realname', 'user_name', 'email', 'user_id']
  tree = ET.parse(gforgeProject + "-users.xml");
  root = tree.getroot();
  u = {}
  for e in root.findall('.//row'):
    user = {}
    for c in list(e):
      if c.get('name') in known_fields:
        user[c.get('name')] = c.text
    u[user['user_name']] = user
  print "%d users found" % len(u)
  return u

def addGforgeUsersToGitLabGroup():
  for user in gForgeUsers:
    addUserToGitLab(user)
    print 'Addding user %s to group %s' % (user, gforgeProject)
    gitLabUser = getGitLabUser(user)
    print gitLabUser
    opResult = requests.post(
        gitLabUrl + 'api/' + gitLabVersion + '/groups/' + gforgeProject + '/members',
        headers = {
            'PRIVATE-TOKEN': gitLabToken,
        },
        data = {
        'user_id': gitLabUser['id'],
        'access_level': 30
        },
        verify = verifySslCertificate
      )
    print opResult

def getIssues():
  known_fields = ['summary', 'user_name', 'details', 'artifact_id', 'status_id', 'open_date', 'assigned_to', 'category_id', 'resolution_id']
  tree = ET.parse(gforgeProject + "-issues.xml");
  root = tree.getroot();
  issues = []
  for e in root.findall('.//row'):
    issue = {}
    for c in list(e):
      if c.get('name') in known_fields:
        issue[c.get('name')] = c.text
    issues.append(issue)
  print "%d issues found" % len(issues)
  return issues

def getCategories():
  issues =  getIssues()
  categories = []
  for issue in issues:
      id = issue['category_id']
      if id not in categories:
        categories.append(id)
  return categories

def getComments():
  known_fields = ['body', 'user_name', 'adddate', 'artifact_id']
  tree = ET.parse(gforgeProject + "-comments.xml");
  root = tree.getroot();
  comments = []
  for e in root.findall('.//row'):
    tracker = {}
    for c in list(e):
      if c.get('name') in known_fields:
        tracker[c.get('name')] = c.text
    comments.append(tracker)
  print "%d comments found" % len(comments)
  return comments

def getFiles():
  known_fields = ['bin_data', 'user_name', 'adddate', 'artifact_id', 'filename']
  tree = ET.parse(gforgeProject + "-files.xml");
  root = tree.getroot();
  attachments = []
  for e in root.findall('.//row'):
    tracker = {}
    for c in list(e):
      if c.get('name') in known_fields:
        tracker[c.get('name')] = c.text
    attachments.append(tracker)
  print "%d attachments found" % len(attachments)
  return attachments

def getClosingActions():
  known_fields = ['artifact_id', 'entrydate', 'user_name']
  tree = ET.parse(gforgeProject + "-issues-history.xml");
  root = tree.getroot();
  closingActions = []
  for e in root.findall('.//row'):
    entry = {}
    for row in list(e):
      if row.get('name') in known_fields:
        entry[row.get('name')] = row.text
        if (row.get('name') == 'entrydate'):
            entrydate = row.text
            if (entrydate):
                val = long(entrydate)
                entry['entrydate'] = val
    closingActions.append(entry)
  return closingActions


def whatisthis(s):
  if isinstance(s, str):
    print 'ascii string'
  elif isinstance(s, unicode):
    print 'unicode string'
  else:
    print 'not a string'

def escapeSQL(body):
    r = u""
    if body:
      try:
        r = body#.decode('iso-8859-1')
      except UnicodeEncodeError as e:
        whatisthis(body)
        whatisthis(r)
        raise
    return r.replace(u'\'', u'\'\'');

def addAttachment(attachment, iid, project_id):
  author = attachment['user_name']
  if '@' in author:
    author = author.split('@')[0]
  _content = StringIO(base64.b64decode(attachment['bin_data']))
  file_info = requests.post(
      gitLabUrl + 'api/' + gitLabVersion + '/projects/%s/uploads' % gitLabProjectId,
      headers = {
          'PRIVATE-TOKEN': gitLabToken,
          'SUDO': author#GITLAB_USER_NAMES.get(author, author)
          },
      files = {
          'file': (
              attachment['filename'],
              _content
          )
      },
      verify = verifySslCertificate
  )
  del _content
  if 'markdown' in file_info.json():
    # now we got the upload URL. Let's post the comment with an
    # attachment
    creationDate = datetime.fromtimestamp(float(attachment['adddate']))
    SQL_File.write(u'INSERT INTO notes (note, noteable_type, author_id, created_at, updated_at, project_id, noteable_id) SELECT \'{note}\', \'Issue\', (SELECT id FROM users WHERE username = \'{username}\'), \'{created_at}\', \'{updated_at}\', {project_id}, (SELECT id FROM issues WHERE iid = {iid} AND project_id = {project_id});\n'.format(note = file_info.json()['markdown'], created_at = creationDate, updated_at = creationDate, username = author, iid = iid, project_id = project_id))
  else:
    print file_info.text
    #raise Exception("'markdown' not found")

def getGitLabUser(username):
  result = requests.get(gitLabApiUrl + '/users?search=%s' % username, headers = {'PRIVATE-TOKEN': gitLabToken},  verify = verifySslCertificate )
  if len(result.json())==0:
    raise Exception("Unable to find user %s in gitlab" % username)
  user = result.json()
  #Some Gitlab versions put a requested user in a list. Some not, so if we get a list, we take the first element.
  if isinstance(user, list):
    user = user[0]
  return user

def addUserToGitLab(name):
    print 'Processing user: %s' % name
    ###return
    if name == 'None':
      return
    domain = 'gforge-migration'
    if '@' in name:
      domain = name.split('@')[1]
      name = name.split('@')[0]
    #We're looking for an existing user
    #We could use a cache here
    resultGitLabUsers = requests.get(gitLabApiUrl + '/users?username=%s' % name, headers = {'PRIVATE-TOKEN': gitLabToken}, verify = verifySslCertificate)
    gitLabUserJson = resultGitLabUsers.json()
    #  if name not in gitLabUsers:
    if len(gitLabUserJson)==0:
    #if it's a known GForge user
      if name in gForgeUsers:
        email = gForgeUsers[name]['email']
        username = gForgeUsers[name]['user_name'];
        name = gForgeUsers[name]['realname']
      else:
        email = name + '@' + domain
        username = name
      print u'Adding user to GitLab ' + name
      addUser = requests.post(
            gitLabApiUrl + '/users',
            headers = {
                  'PRIVATE-TOKEN': gitLabToken,
                  },
            verify = verifySslCertificate,
            data = {
                'email': email,
                'name': name,
                'username': username,
                'provider': 'ldapmain',
                'extern_uid': 'uid=' + username + ',ou=people,dc=ow2,dc=org',
                'skip_confirmation': True,
                'reset_password': True
            }
        )
      if addUser.status_code != 201:
        # print gForgeUsers[name]['email']
        print addUser.text
        #raise Exception
      #We check that the new user is saved
      resultGitLabUsers = requests.get(gitLabApiUrl + '/users?search=%s' % username, headers = {'PRIVATE-TOKEN': gitLabToken},  verify = verifySslCertificate )
      if len(resultGitLabUsers.json())==0:
        raise Exception("Unable to find user %s in gitlab" % username)
      gitLabUserJson = resultGitLabUsers.json()
      #Some Gitlab versions put a requested user in a list. Some not, so if we get a list, we take the first element.
      if isinstance(gitLabUserJson, list):
        gitLabUserJson = gitLabUserJson[0]
    else:
      #the user is known in Gitlab, but we still have to verify.
      #Some Gitlab versions put a requested user in a list. Some not, so if we get a list, we take the first element.
        if isinstance(gitLabUserJson, list):
          gitLabUserJson = gitLabUserJson[0]
        if name in gForgeUsers:
          if 'realname' in gForgeUsers[name] and (gitLabUserJson['name'] != gForgeUsers[name]['realname']):
            modGitUser = requests.put(
            gitLabApiUrl + '/users/%s' % gitLabUserJson['id'],
            headers = {
                'PRIVATE-TOKEN': gitLabToken,
                },
            verify = verifySslCertificate,
            data = {
                'name': gForgeUsers[name]['realname'],
                'email': gForgeUsers[name]['email'],
                'username': gForgeUsers[name]['user_name'],
                'provider': 'ldapmain',
                'extern_uid': 'uid=' + gForgeUsers[name]['user_name'] + ',ou=people,dc=ow2,dc=org',
                'skip_confirmation': True,
                'reset_password': True
            })
#            print modGitUser.text
#            raise Exception("User mismatch %s %s") % (gitLabUserJson[0]['name'], gForgeUsers[reporter]['displayName'])
#    print resultGitLabUsers.text

    #print 'Adding user to project %s' % gitLabProjectId
    #if gitLabUserJson['id'] not in projectUsers:
      #print 'Adding user ******************** ' + gitLabUserJson['name']
      #addUserToProject = requests.post(gitLabApiUrl + '/projects/%s/members' % gitLabProjectId,
        #headers = {'PRIVATE-TOKEN': gitLabToken}, verify = verifySslCertificate, data = {'user_id': gitLabUserJson['id'], 'access_level': 10})
      #if addUserToProject.status_code not in [201, 409]:
        #print addUserToProject.url
        #print addUserToProject.text
        #print gForgeUsers[name]['emailAddress']
        #raise Exception ("User %s not added" % gitLabUserJson['name'])
      #else:
        #projectUsers.append(gitLabUserJson['id'])
    #HINT: if there is a need of removing notifications on a project, here is the code:
    #removeNotifications = requests.put(
        #gitLabUrl + 'api/' + gitLabVersion + '/notification_settings?level=disabled',
        #headers={
               #'PRIVATE-TOKEN': gitLabToken,
               #'SUDO': name,
               #},
        #verify=verifySslCertificate,
        #data={
            #'user_id': gitLabUserJson['id'],
        #})
    #if removeNotifications.status_code not in [200]:
      #print removeNotifications.status_code
      #print removeNotifications.url
      #print removeNotifications.text
      #print gForgeUsers[name]['emailAddress']
      #raise Exception ("User %s notification level couldn't be changed." % gitLabUserJson['name'])
    gitLabUsers[name] = gitLabUserJson

reservedNames = ['admin']

def translateUsername(gforgeName):
  gitlabName = escapeSQL(gforgeName)
  if gitlabName in reservedNames:
    gitlabName = 'GForge' + gitlabName
  return gitlabName

def convertAssignedTo(assignedTo):
    return ''

def processIssue(issue):
  reporter = translateUsername(issue['user_name'])
  if (reporter == 'None'):
    reporter = 'anonymous'
  #description = '```\n' + escapeSQL(issue['details']) + '\n```'
  description = escapeSQL(issue['details'])
  title = escapeSQL(issue['summary'])
  iid = issue['artifact_id']
  resolutionId = issue['resolution_id']
  issueResolution[iid] = resolutionMap[int(resolutionId)]
  print 'Processing issue #%s' % iid
  categoryId = issue['category_id']
  assignedTo = issue['assigned_to']
  assigneeId = convertAssignedTo(assignedTo)
  categoryLabel = None;
  #categoryLabel = Config.get('ASM', categoryId)
  #categoryFilter = Config.get('GForge', 'categories')
  #if (categoryLabel not in categoryFilter):
  #    return

  issues.append(issue['artifact_id'])
  if issue['status_id'] == '2':
    state = 'closed'
  else:
    state = 'opened'
  creationDate = datetime.fromtimestamp(float(issue['open_date']))
  addUserToGitLab(reporter)
  if '@' in reporter:
    reporter = reporter.split('@')[0]
  try:
    SQL = u'INSERT INTO issues (title, project_id, iid, author_id, state, created_at, updated_at, description) SELECT \'{title}\', {project_id}, {iid}, id, \'{state}\', \'{created_at}\', \'{updated_at}\', \'{description}\' FROM users WHERE username = \'{username}\';\n'.format(title = title, iid = iid, project_id = gitLabProjectId, state = state, created_at = creationDate.isoformat(), updated_at = creationDate.isoformat(), username = reporter, description = description)
    SQL_File.write(SQL)
    if categoryLabel != 'None':
      SQL_File.write(u'INSERT INTO label_links (label_id, target_id, target_type) (SELECT l.id, i.id, \'Issue\' FROM labels l, issues i WHERE l.title = \'{label}\' AND iid = {iid} AND l.project_id = {project_id} AND i.project_id = {project_id});\n'.format(label = categoryLabel, iid = iid, project_id = gitLabProjectId));
    print "SQL generated for issue #%s" % iid
  except UnicodeEncodeError as e:
    print title
    print iid
    print reporter
    print description.encode('utf8', 'ignore')
    raise
  except UnicodeDecodeError as e:
    print title
    print iid
    print reporter
    print description.encode('utf8', 'ignore')
    raise

def processComment(comment):
  author = translateUsername(comment['user_name'])
  #body = '```\n' + escapeSQL(comment['body']) + '\n```'
  body = escapeSQL(comment['body'])
  addUserToGitLab(author)
  if '@' in author:
    author = author.split('@')[0]
  creationDate = datetime.fromtimestamp(float(comment['adddate']))
  SQL_File.write(u'INSERT INTO notes (note, noteable_type, author_id, created_at, updated_at, project_id, noteable_id) SELECT \'{note}\', \'Issue\', (SELECT id FROM users WHERE username = \'{username}\'), \'{created_at}\', \'{updated_at}\', {project_id}, (SELECT id FROM issues WHERE iid = {iid} AND project_id = {project_id});\n'.format(note = body, project_id = gitLabProjectId, created_at = creationDate.isoformat(), updated_at = creationDate.isoformat(), username = author, iid = comment['artifact_id']))
    # if len(issue_info['fields']['attachment']):
      # print "Attachement found"
      # for attachment in issue_info['fields']['attachment']:
        # addAttachment(attachment, iid, gitLabProjectId)

def processAttachment(attachment):
  print 'Processing attachment'
  ###return
  author = translateUsername(attachment['user_name'])
  addUserToGitLab(author)
  if '@' in author:
    author = author.split('@')[0]
  addAttachment(attachment, attachment['artifact_id'], gitLabProjectId)

if not gitLabProject:
  createGroup = requests.post(
    gitLabUrl + 'api/' + gitLabVersion + '/groups',
    headers = {
      'PRIVATE-TOKEN': gitLabToken,
      },
    data = {
      'name': gforgeProject.lower(),
      'path': gforgeProject.lower()
    },
    verify = verifySslCertificate
  )
  print createGroup.text
  createProject = requests.post(
    gitLabUrl + 'api/' + gitLabVersion + '/projects',
    headers = {
      'PRIVATE-TOKEN': gitLabToken,
      },
    data = {
      'path': gforgeProject.lower()
    },
    verify = verifySslCertificate
  )
  print createProject.text
  gitLabProjectId = createProject.json()['id']

  transfertProjectToGroup = requests.post(
    gitLabUrl + 'api/' + gitLabVersion + '/groups/' + str(createGroup.json()['id']) + '/projects/' + str(createProject.json()['id']),
    headers={
      'PRIVATE-TOKEN': gitLabToken,
      },
    data={
      'name': gforgeProject.lower(),
      'path': gforgeProject.lower()
    },
    verify=verifySslCertificate
  )
  gitLabProject = gforgeProject.lower() + "/" + gforgeProject.lower()
  print transfertProjectToGroup.text

  notifSetting = requests.put(
    gitLabUrl + 'api/' + gitLabVersion + '/projects/' + str(createProject.json()['id']) + '/notification_settings',
    headers={
      'PRIVATE-TOKEN': gitLabToken,
      },
    data={
      'level': 'disabled'  },
    verify=verifySslCertificate
  )
  print notifSetting.text

def processClosingAction(action):
  user = action['user_name']
  iid = action['artifact_id']
  if iid not in issues:
    return
  if iid not in issueResolution:
    print 'Artifact ID ' + iid + ' not found in the resolution map'
    return
  resolution = issueResolution[iid]
  creationDate = datetime.fromtimestamp(action['entrydate'])
  ##locale.setlocale(locale.LC_TIME,'')
  dateStr = creationDate.strftime('%Y-%d-%d')

  if resolution != 'None':
    body = 'Closing as ' + resolution + '.'
    SQL_File.write(u'INSERT INTO notes (note, noteable_type, author_id, created_at, updated_at, project_id, noteable_id) SELECT \'{note}\', \'Issue\', (SELECT id FROM users WHERE username = \'{username}\'), \'{created_at}\', \'{updated_at}\', {project_id}, (SELECT id FROM issues WHERE iid = {iid} AND project_id = {project_id});\n'.format(note = body, project_id = gitLabProjectId, created_at = creationDate.isoformat(), updated_at = creationDate.isoformat(), username = user, iid = action['artifact_id']))

print 'Getting the project id of project %s' % gitLabProject
#    find out the ID of the project.
def getProjectId(projectName):
    gitLabProjectId =  None
    for project in requests.get(gitLabUrl + 'api/' + gitLabVersion + '/projects', headers={'PRIVATE-TOKEN': gitLabToken}).json():
      if project['path_with_namespace'] == gitLabProject:
        gitLabProjectId = project['id']
        return gitLabProjectId
        break
    if not gitLabProjectId:
      raise Exception("Unable to find %s in gitlab!" % gitLabProject)

issueResolution = {}
issues = []
gitLabProjectId = getProjectId(gitLabProject)
gForgeUsers = getUsers()
print gForgeUsers
gitLabUsers = {}
projectUsers = []
addGforgeUsersToGitLabGroup()

importCategoriesAsLabels()

resolutionMap = {}
resolutionMap[1] = 'fixed'
resolutionMap[2] = 'invalid'
resolutionMap[3] = 'wont fix'
resolutionMap[4] = 'later'
resolutionMap[5] = 'remind'
resolutionMap[6] = 'works for me'
resolutionMap[100] = 'None'
resolutionMap[101] = 'duplicate'
resolutionMap[102] = 'accepted'
resolutionMap[103] = 'out of date'
resolutionMap[104] = 'postponed'
resolutionMap[105] = 'rejected'

for issue in getIssues():
  processIssue(issue)
# #print "imported %s issues from project %s" % (len(issues), JIRA_PROJECT


for comment in getComments():
  if (comment['artifact_id'] in issues):
    processComment(comment)
# #print "imported %s comments from project %s" % (len(comments), JIRA_PROJECT)
for attachment in getFiles():
  processAttachment(attachment)

closedIssues = []

for closingAction in getClosingActions():
  if (closingAction['artifact_id'] not in closedIssues):
    processClosingAction(closingAction)
    closedIssues.append(closingAction['artifact_id'])

#Replace links to issues
SQL_File.write(u'UPDATE issues SET description = regexp_replace(description, \'http://forge.ow2.org/tracker/\\?func=detail&aid=([^&]*)[^\\s]*\', \'#\\1\', \'g\') WHERE project_id = {project_id};\n'.format(project_id = gitLabProjectId))
SQL_File.write(u'UPDATE notes SET note = regexp_replace(note, \'http://forge.ow2.org/tracker/\\?func=detail&aid=([^&]*)[^\\s]*\', \'#\\1\', \'g\'), note_html = NULL WHERE noteable_type = \'Issue\' AND noteable_id IN (SELECT id FROM issues WHERE project_id = {project_id});\n'.format(project_id = gitLabProjectId))
#Remove 'Logged In: YES user_id=xxx'
SQL_File.write(u'UPDATE notes SET note = regexp_replace(note, \'Logged In: YES\s\n\', \'\', \'g\'), note_html = NULL WHERE noteable_type = \'Issue\' AND noteable_id IN (SELECT id FROM issues WHERE project_id = {project_id});\n'.format(project_id = gitLabProjectId))
SQL_File.write(u'UPDATE notes SET note = regexp_replace(note, \'user_id=[\\d]*\n\n\', \'\', \'g\'), note_html = NULL WHERE noteable_type = \'Issue\' AND noteable_id IN (SELECT id FROM issues WHERE project_id = {project_id});\n'.format(project_id = gitLabProjectId))
