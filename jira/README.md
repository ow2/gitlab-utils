This project makes it possible to import JIRA issues into GitLab. It uses the JIRA REST API to export issues, then it converts the data into SQL code, makes some transformation and produces an SQL script ready to be executed on a GitLab instance.

WARNING : you *should* disable email notifications subsystem in `/etc/gitlab/gitlab.rb` before running the script to prevent gitlab sending email to newly created users:

```
### Email Settings
gitlab_rails['gitlab_email_enabled'] = false
```

- copy config.cfg.dist to config.cfg update configuration values: jiraProject and gitLabProject. Set gitLabProject to None if the gitlab project isn't created, the script will create it, else its name should be JIRA_PROJECT.lower().
- gitLabUrl contains the URL of the GitLab instance.
- gitLabToken is the 'private token' of the GitLab instance administrator (User Avatar menu > Settings > Account tab - NB : it is related to Access token tab) .
- The users accounts created in gitlab will contains

## Converting and importing issues

- Run the SQL generator: ```python SqlGenerator.py```
- Upload the generated file to the GitLab server: ```scp project-issues.sql %username%@%gitlab-server%:/%path%```

- On the GitLab server: ```sudo -u gitlab-psql /opt/gitlab/embedded/bin/psql -h /var/opt/gitlab/postgresql gitlabhq_production -f %project-issues.sql%```

The variable "acquiredIssues" counts the issues acquired by the script. To start further, set the value to the index of the new start.

## Useful SQL commands :

Delete issues for a project :

```
DELETE FROM issues WHERE project_id = %PROJECT_ID;
DELETE FROM notes WHERE noteable_type = 'Issue' AND noteable_id NOT IN (SELECT id FROM issues);
```
