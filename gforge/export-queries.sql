define GROUP_ID=23;
-- Retrieve users for one group
SELECT * FROM users u, user_group ug WHERE group_id = 23 AND u.user_id = ug.user_id;
-- Retrieve issues for one group.
SELECT A.*, U.user_name FROM artifact A, artifact_group_list AGL, users U WHERE AGL.group_id = 23 AND A.group_artifact_id = AGL.group_artifact_id AND A.status_id <> '3' AND U.user_id = A.submitted_by;
-- Retrieve answers to issues
SELECT AM.*, U.user_name FROM artifact_message AM, artifact A, artifact_group_list AGL, users U WHERE A.artifact_id = AM.artifact_id AND AGL.group_id = 23 AND A.group_artifact_id = AGL.group_artifact_id AND U.user_id = AM.submitted_by AND A.status_id <> '3' ORDER BY AM.adddate;
-- Retrieve files of issues
SELECT AF.*, U.user_name FROM artifact_file AF, artifact A, artifact_group_list AGL, users U WHERE A.artifact_id = AF.artifact_id AND AGL.group_id = 23 AND A.group_artifact_id = AGL.group_artifact_id AND U.user_id = AF.submitted_by AND A.status_id <> '3' ORDER BY AF.adddate;
-- Retrieve issue history
SELECT H.*, U.user_name FROM artifact_history H, artifact A, artifact_group_list AGL, users U WHERE H.artifact_id = A.artifact_id and AGL.group_id = 23 AND A.group_artifact_id = AGL.group_artifact_id AND U.user_id = H.mod_by and H.field_name='close_date';
